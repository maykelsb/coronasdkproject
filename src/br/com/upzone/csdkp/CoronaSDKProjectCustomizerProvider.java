package br.com.upzone.csdkp;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.spi.project.ui.CustomizerProvider;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.openide.awt.StatusDisplayer;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Maykel dos Santos Braz <maykelsb@yahoo.com.br>
 */
public class CoronaSDKProjectCustomizerProvider implements CustomizerProvider {

  public final CoronaSDKProject project;
  public static final String CUSTOMIZER_FOLDER_PATH = "Projects/br-com-upzone-csdkp/Customizer";

  public CoronaSDKProjectCustomizerProvider (CoronaSDKProject project) {
    this.project = project;
  }

  @Override
  public void showCustomizer() {
    Dialog dialog = ProjectCustomizer.createCustomizerDialog(CUSTOMIZER_FOLDER_PATH, Lookups.fixed(project), "", new OKOptionListener(), null);
    dialog.setTitle(ProjectUtils.getInformation(project).getDisplayName());
    dialog.setVisible(true);
  }

  private class OKOptionListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      StatusDisplayer.getDefault().setStatusText("Ok button clicked for "
              + project.getProjectDirectory().getName() + " customizer!");
    }
  }
}
