package br.com.upzone.csdkp;

/*
 * 
 */
import java.awt.Image;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import org.netbeans.api.annotations.common.StaticResource;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.spi.project.ProjectState;
import org.netbeans.spi.project.ui.CustomizerProvider;
import org.netbeans.spi.project.ui.LogicalViewProvider;
import org.netbeans.spi.project.ui.support.CommonProjectActions;
import org.netbeans.spi.project.ui.support.NodeFactorySupport;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

/**
 *
 * @author Maykel dos Santos Braz <maykelsb@yahoo.com.br>
 */
public class CoronaSDKProject implements Project {

  private final FileObject projectDir;
  private final ProjectState state;
  private Lookup lkp;

  CoronaSDKProject(FileObject dir, ProjectState state) {
    this.projectDir = dir;
    this.state = state;
  }

  @Override
  public FileObject getProjectDirectory() {
    return projectDir;
  }

  @Override
  public Lookup getLookup() {
    if (null == lkp) {
      lkp = Lookups.fixed(new Object[]{
        // -- Register your features here
        this,
        new Info(),
        new LogicalView(this),
        new CoronaSDKProjectCustomizerProvider(this),
      });
    }
    return lkp;
  }

  private final class Info implements ProjectInformation {

    @StaticResource()
    public static final String CSDKP_ICON = "br/com/upzone/csdkp/icon.png";

    @Override
    public Icon getIcon() {
      return new ImageIcon(ImageUtilities.loadImage(CSDKP_ICON));
    }

    @Override
    public String getName() {
      return getProjectDirectory().getName();
    }

    @Override
    public String getDisplayName() {
      return getName();
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener pcl) {
      //do nothing
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener pcl) {
      //do nothing
    }

    @Override
    public Project getProject() {
      return CoronaSDKProject.this;
    }
  }

  private final class LogicalView implements LogicalViewProvider {

    @StaticResource()
    public static final String CSDKP_ICON = "br/com/upzone/csdkp/icon.png";
    private final CoronaSDKProject project;

    public LogicalView(CoronaSDKProject project) {
      this.project = project;
    }

    @Override
    public Node createLogicalView() {
      try {
        // -- Obtendo o node do diretório do projeto
        FileObject prjDirectory = project.getProjectDirectory();
        DataFolder prjFolder = DataFolder.findFolder(prjDirectory);
        Node nodeOfPrjFolder = prjFolder.getNodeDelegate();

        // -- Decorando o node do diretório do projeto
        return new ProjectNode(nodeOfPrjFolder, project);
      } catch (DataObjectNotFoundException donfe) {
        // -- O node do diretório não poder ser criada: ready only ou algo ruim aconteceu
        return new AbstractNode(Children.LEAF);
      }
    }

    private final class ProjectNode extends FilterNode {
      final CoronaSDKProject project;

      public ProjectNode(Node node, CoronaSDKProject project) throws DataObjectNotFoundException {
        super(node,
              NodeFactorySupport.createCompositeChildren(project, "Projects/br-com-upzone-csdkp"),
              new ProxyLookup(
                new Lookup[] {
                  Lookups.singleton(project),
                  node.getLookup()
                }));
        this.project = project;
      }

      @Override
      public Action[] getActions(boolean arg0) {
        return new Action[]{
          CommonProjectActions.newFileAction(),
          CommonProjectActions.copyProjectAction(),
          CommonProjectActions.deleteProjectAction(),
          CommonProjectActions.customizeProjectAction(),
          CommonProjectActions.closeProjectAction()

        };
      }

      @Override
      public Image getIcon(int type) {
        return ImageUtilities.loadImage(CSDKP_ICON);
      }

      @Override
      public Image getOpenedIcon(int type) {
        return getIcon(type);
      }

      @Override
      public String getDisplayName() {
        return project.getProjectDirectory().getName();
      }
    }

    @Override
    public Node findPath(Node root, Object target) {
      return null;
    }
  }
}