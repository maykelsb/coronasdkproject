package br.com.upzone.csdkp.nodes;

import br.com.upzone.csdkp.CoronaSDKProject;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.NodeFactory;
import org.netbeans.spi.project.ui.support.NodeList;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

/**
 * Implementação da listagem do nó "docs" no projeto.
 * @author Maykel dos Santos Braz <maykelsb@yahoo.com.br>
 */
@NodeFactory.Registration(projectType = "br-com-upzone-csdkp", position = 15)
public class DocsNodeFactory implements NodeFactory {

  @Override
  public NodeList<?> createNodes(Project project) {
    CoronaSDKProject p = project.getLookup().lookup(CoronaSDKProject.class);
    assert p != null;
    return new DocsNodeList(p);
  }

  private class DocsNodeList implements NodeList<Node> {
    CoronaSDKProject project;

    public DocsNodeList(CoronaSDKProject project) {
      this.project = project;
    }

    @Override
    public List<Node> keys() {
      FileObject docsFolder = project.getProjectDirectory().getFileObject("docs");
      List<Node> result = new ArrayList<Node>();
      if (docsFolder != null) {
        for (FileObject docsFolderFile:docsFolder.getChildren()) {
          try {
            result.add(DataObject.find(docsFolderFile).getNodeDelegate());
          } catch (DataObjectNotFoundException ex) {
            Exceptions.printStackTrace(ex);
          }
        }
      }
      return result;
    }

    @Override
    public Node node(Node node) {
      return new FilterNode(node);
    }

    @Override
    public void addNotify() {
    }

    @Override
    public void removeNotify() {
    }

    @Override
    public void addChangeListener(ChangeListener cl) {
    }

    @Override
    public void removeChangeListener(ChangeListener cl) {
    }
  }
}
